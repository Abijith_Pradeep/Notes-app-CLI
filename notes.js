const fs = require('fs')

var fetchNote = () => {
    try {
        var notesString = fs.readFileSync('notes-data.json')
        return JSON.parse(notesString)
    }
    catch (e) {
        return []
    }
}

var addNote = (title, body) => {
    console.log('Adding note with Title :' + (title) + 'and Body : ' + (body))
    var note = {
        title,
        body 
    }
    notes = fetchNote()
    //Checking if title already exits
    var duplicateNotes = notes.filter((note) => note.title === title)
    if (duplicateNotes.length > 0) {
        console.log("Title already exists. Give a different title")
        process.exit() 
        }
    //Saving the note
    notes.push(note)
    console.log("New node added ")
    console.log("_ _ _")
    console.log(`Title : ${note.title}`)
    console.log(`Body : ${note.body}`)
    fs.writeFileSync('notes-data.json',JSON.stringify(notes))
    return note
}

var getAll = () => {
    console.log('Getting all nodes')
    notes = fetchNote()
    if (notes.length == 0) {
        console.log("No notes found !")
    }
    for (var i = 0; i < notes.length; i++) {
        console.log("_ _ _")
        console.log()
        console.log(`Title : ${notes[i].title}`)
        console.log(`Body : ${notes[i].body}`)
    }
}

var getNote = (title) => {
    notes = fetchNote()
    var Notfound = true
    for (var i = 0; i < notes.length; i++) {
        if (notes[i].title === title) { 
            console.log("_ _ _")
            console.log(`Title : ${notes[i].title}`)
            console.log(`Body : ${notes[i].body}`)
            Notfound = false
            }
        }
    if (Notfound) {
        console.log("No such Note found !")
    }
}

var removeNote = (title) => {
    notes = fetchNote()
    var temp = notes.filter((note) => note.title !== title)
    if (temp.length < notes.length){
        fs.writeFileSync('notes-data.json',JSON.stringify(temp))
        console.log("Note successfully removed")
    }
    else {
        console.log("Note not found !")
    }
}
module.exports = {
    addNote, //This is identical to addNote : addNote
    getAll,
    getNote,
    removeNote
}