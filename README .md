<h1>Notes App CLI</h1>
This is a **CLI application built with NodeJS to store and maintain Notes**. Users can add, remove, read and display all notes.
All commands are given as command line arguments along with title and body of the notes.

<h3>Pre-requisites :</h3>
    1. Install NodeJS 
    2. Perfom " node install " command inside the cloned repository. This will automatically install all the required modules.

<h3>Usage pattern :</h3>
    1. node filename --command add --title "Title of the note" --body "Body of the note" 
    2. node filename --command remove --title "Title of the note"
    3. node filename --command read --title "Title of the note"
    4. node filename --command list 
    
